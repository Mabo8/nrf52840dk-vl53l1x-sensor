# VL53l1x sensor / nrf52840dk
The program is based on the example code **twi_sensor** from [nrf5 SDK](https://infocenter.nordicsemi.com/topic/struct_sdk/struct/sdk_nrf5_latest.html).    
For the sensor I used the [ultra lite driver doc](https://www.st.com/resource/en/user_manual/dm00562924-a-guide-to-using-the-vl53l1x-ultra-lite-driver-stmicroelectronics.pdf) from st.
## Requirements 
### hardware
* [nrf52480 DK](https://www.nordicsemi.com/Software-and-Tools/Development-Kits/nRF52840-DK)
* [vl53l1x](https://www.waveshare.com/vl53l1x-distance-sensor.htm) other manufacture are suitable as well, but check the electrical requirements first before you use other vl53l1x boards.
* [logic level converter](https://www.adafruit.com/product/757) 

### software
* [GNU ARM Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
* [nrf connect](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-desktop)
   
## How to start
### hardware
**Important: always check voltages before you connect something!**
* the [vl53l1x](https://www.waveshare.com/vl53l1x-distance-sensor.htm) from waveshare needs a 5V logic signal for i2c, nrf52840 offers just around ~ 3V logic level. Therefore use the [logic level converter](https://www.adafruit.com/product/757) between board and sensor
* connect pin 0.27(SCL), 0.26(SDA), GND and VDD (~ 3V) from nrf52480 to LV side of logic level converter 
* connect GND and 5V from nrf52480 to HV side of logic level converter
* connect GND, 5V, SDA and SCL from HV side to vl53l1x

### software
* download [GNU ARM Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) and save it in a global folder in case you want to use it within other projects.
* open you projectfolder with VS Code (or similar IDE)
* add the location of your GNU toolchain to ```nrf52840dk-vl53l1x-sensor/components/toolchain/gcc/Makefile.posix```
* navigate to ```nrf52840dk-vl53l1x-sensor/vl53l1x_sensor/pca10056/blank/armgcc```
   * open Makefile
   * ```PROJ_DIR``` - add path of project directory
* open terminal in Makefile folder and execute ```make```
* open [nrf connect](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Connect-for-desktop)
   * select programmer
   * select device
   * add your .hex (can be found in dir ```_built```)
   * click *Erase & write*

## Program test
* use PuTTY or a similar program
* choose *Serial* and add serial name (e.g. /dev/ttyACM0) and speed (115200)
   * data bits (8), stop bits (1), parity (none), flow control (XON/XOFF)
* click *open*
* after the initialization the measurement starts 