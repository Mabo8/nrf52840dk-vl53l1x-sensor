#include <stdio.h>
#include <string.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* TWI instance ID. */
#define TWI_INSTANCE_ID     0

#define VL53L1X_ADDR        0x29U //from scan

/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

uint8_t bufferRX;
uint8_t bufferTX;

uint8_t rangeStatus;	/*!< ResultStatus */
uint16_t rangeDistance;	/*!< ResultDistance */

#include "vl53l1x_sensor.h"

__STATIC_INLINE void data_handler(uint8_t newValue)
{
    //NRF_LOG_INFO("New value: %d ", newValue);
    return;
}

void twi_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
    switch (p_event->type)
    {
        case NRF_DRV_TWI_EVT_DONE:
            if (p_event->xfer_desc.type == NRF_DRV_TWI_XFER_RX)
            {
                data_handler(bufferRX);
            }
            break;
        default:
            break;
    }
}

void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_vl53l1x_config = {
       .scl                = ARDUINO_SCL_PIN,
       .sda                = ARDUINO_SDA_PIN,
       .frequency          = NRF_DRV_TWI_FREQ_100K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_vl53l1x_config, twi_handler, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}

void distanceRead(){
    
	nrf_drv_twi_rx(&m_twi, VL53L1X_ADDR, &bufferRX, sizeof(bufferRX));
}

int main(void)
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    NRF_LOG_INFO("\r\nvl53l1x example started.");
    NRF_LOG_FLUSH();
    twi_init();
    nrf_delay_ms(500); 

    VL53L1X_SensorInit(); 
    VL53L1X_StartRanging();

    while (true)
    {
        nrf_delay_ms(1000);
        doMeasurment();
        // NRF_LOG_INFO("range status: (%d)", rangeStatus);
        // NRF_LOG_FLUSH(); 
        nrf_delay_ms(50);
        NRF_LOG_INFO("Distance: (%d)", rangeDistance);
        NRF_LOG_FLUSH();
        //systemStatus();
    }
}
