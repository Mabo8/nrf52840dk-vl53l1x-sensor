#ifndef _VL53L1X_SENSOR_
#define _VL53L1X_SENSOR_

#include <string.h>

#define SOFT_RESET											0x0000
#define VL53L1_I2C_SLAVE__DEVICE_ADDRESS					0x0001
#define VL53L1_VHV_CONFIG__TIMEOUT_MACROP_LOOP_BOUND        0x0008
#define ALGO__CROSSTALK_COMPENSATION_PLANE_OFFSET_KCPS 		0x0016
#define ALGO__CROSSTALK_COMPENSATION_X_PLANE_GRADIENT_KCPS 	0x0018
#define ALGO__CROSSTALK_COMPENSATION_Y_PLANE_GRADIENT_KCPS 	0x001A
#define ALGO__PART_TO_PART_RANGE_OFFSET_MM_HI				0x001E
#define ALGO__PART_TO_PART_RANGE_OFFSET_MM_LO				0x001F
#define MM_CONFIG__INNER_OFFSET_MM							0x0020
#define MM_CONFIG__OUTER_OFFSET_MM_HI						0x0022
#define MM_CONFIG__OUTER_OFFSET_MM_LO						0x0023
#define PAD_I2C_HV__CONFIG                                  0x002D
#define PAD_I2C_HV__EXTSUP_CONFIG                           0x002E
#define GPIO_HV_MUX__CTRL									0x0030
#define GPIO__TIO_HV_STATUS       							0x0031
#define SIGMA_ESTIMATOR__EFFECTIVE_PULSE_WIDTH_NS           0x0036
#define SIGMA_ESTIMATOR__EFFECTIVE_AMBIENT_WIDTH_NS         0x0037
#define ALGO__CROSSTALK_COMPENSATION_VALID_HEIGHT_MM        0x0039
#define ALGO__RANGE_IGNORE_VALID_HEIGHT_MM                  0x003E
#define ALGO__RANGE_MIN_CLIP                                0x003F
#define ALGO__CONSISTENCY_CHECK__TOLERANCE                  0x0040
#define SYSTEM__INTERRUPT_CONFIG_GPIO 						0x0046
#define PHASECAL_CONFIG__TIMEOUT_MACROP     				0x004B
#define DSS_CONFIG__ROI_MODE_CONTROL                        0x004F
#define SYSTEM__THRESH_RATE_HIGH_HI                         0x0050
#define SYSTEM__THRESH_RATE_HIGH_LO                         0x0051
#define SYSTEM__THRESH_RATE_LOW_HI                          0x0052
#define SYSTEM__THRESH_RATE_LOW_LO                          0x0053
#define DSS_CONFIG__MANUAL_EFFECTIVE_SPADS_SELECT_HI        0x0054
#define DSS_CONFIG__MANUAL_EFFECTIVE_SPADS_SELECT_LO        0x0055
#define DSS_CONFIG__APERTURE_ATTENUATION                    0x0057
#define RANGE_CONFIG__TIMEOUT_MACROP_A_HI   				0x005E
#define RANGE_CONFIG__TIMEOUT_MACROP_A_LO   				0x005F
#define RANGE_CONFIG__VCSEL_PERIOD_A        				0x0060
#define RANGE_CONFIG__VCSEL_PERIOD_B						0x0063
#define RANGE_CONFIG__TIMEOUT_MACROP_B_HI  					0x0061
#define RANGE_CONFIG__TIMEOUT_MACROP_B_LO  					0x0062
#define RANGE_CONFIG__SIGMA_THRESH_HI                       0x0064
#define RANGE_CONFIG__SIGMA_THRESH_LO                       0x0065
#define RANGE_CONFIG__MIN_COUNT_RATE_RTN_LIMIT_MCPS_HI      0x0066
#define RANGE_CONFIG__MIN_COUNT_RATE_RTN_LIMIT_MCPS_LO      0x0067
#define RANGE_CONFIG__VALID_PHASE_HIGH      				0x0069
#define SYSTEM__INTERMEASUREMENT_PERIOD_3                   0x006C
#define SYSTEM__INTERMEASUREMENT_PERIOD_2                   0x006D
#define SYSTEM__INTERMEASUREMENT_PERIOD_1                   0x006E
#define SYSTEM__INTERMEASUREMENT_PERIOD_0                   0x006F
#define SYSTEM__GROUPED_PARAMETER_HOLD_0                    0x0071
#define SYSTEM__THRESH_HIGH 								0x0072
#define SYSTEM__THRESH_LOW 									0x0074
#define SYSTEM__SEED_CONFIG                                 0x0077
#define SD_CONFIG__WOI_SD0_HI                  				0x0078
#define SD_CONFIG__WOI_SD0_LO                  				0x0079
#define SD_CONFIG__INITIAL_PHASE_SD0_HI        				0x007A
#define SD_CONFIG__INITIAL_PHASE_SD0_LO        				0x007B
#define SYSTEM__GROUPED_PARAMETER_HOLD_1                    0x007C
#define SD_CONFIG__QUANTIFIER                               0x007E
#define ROI_CONFIG__USER_ROI_CENTRE_SPAD					0x007F
#define ROI_CONFIG__USER_ROI_REQUESTED_GLOBAL_XY_SIZE		0x0080
#define SYSTEM__SEQUENCE_CONFIG								0x0081
#define VL53L1_SYSTEM__GROUPED_PARAMETER_HOLD 				0x0082
#define SYSTEM__INTERRUPT_CLEAR       						0x0086
#define SYSTEM__MODE_START                 					0x0087
#define VL53L1_RESULT__RANGE_STATUS							0x0089
#define VL53L1_RESULT__DSS_ACTUAL_EFFECTIVE_SPADS_SD0		0x008C
#define RESULT__AMBIENT_COUNT_RATE_MCPS_SD					0x0090
#define VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0_HI			0x0096
#define VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0_LO			0x0097
#define VL53L1_RESULT__PEAK_SIGNAL_COUNT_RATE_CROSSTALK_CORRECTED_MCPS_SD0 	0x0098
#define VL53L1_RESULT__OSC_CALIBRATE_VAL_HI					0x00DE
#define VL53L1_RESULT__OSC_CALIBRATE_VAL_LO					0x00DF
#define VL53L1_FIRMWARE__SYSTEM_STATUS                      0x00E5
#define VL53L1_IDENTIFICATION__MODEL_ID                     0x010F
#define VL53L1_ROI_CONFIG__MODE_ROI_CENTRE_SPAD				0x013E


const uint8_t VL51L1X_DEFAULT_CONFIGURATION[] = {
0x00, /* 0x2d : set bit 2 and 5 to 1 for fast plus mode (1MHz I2C), else don't touch */
0x00, /* 0x2e : bit 0 if I2C pulled up at 1.8V, else set bit 0 to 1 (pull up at AVDD) */
0x00, /* 0x2f : bit 0 if GPIO pulled up at 1.8V, else set bit 0 to 1 (pull up at AVDD) */
0x01, /* 0x30 : set bit 4 to 0 for active high interrupt and 1 for active low (bits 3:0 must be 0x1), use SetInterruptPolarity() */
0x02, /* 0x31 : bit 1 = interrupt depending on the polarity, use CheckForDataReady() */
0x00, /* 0x32 : not user-modifiable */
0x02, /* 0x33 : not user-modifiable */
0x08, /* 0x34 : not user-modifiable */
0x00, /* 0x35 : not user-modifiable */
0x08, /* 0x36 : not user-modifiable */
0x10, /* 0x37 : not user-modifiable */
0x01, /* 0x38 : not user-modifiable */
0x01, /* 0x39 : not user-modifiable */
0x00, /* 0x3a : not user-modifiable */
0x00, /* 0x3b : not user-modifiable */
0x00, /* 0x3c : not user-modifiable */
0x00, /* 0x3d : not user-modifiable */
0xff, /* 0x3e : not user-modifiable */
0x00, /* 0x3f : not user-modifiable */
0x0F, /* 0x40 : not user-modifiable */
0x00, /* 0x41 : not user-modifiable */
0x00, /* 0x42 : not user-modifiable */
0x00, /* 0x43 : not user-modifiable */
0x00, /* 0x44 : not user-modifiable */
0x00, /* 0x45 : not user-modifiable */
0x20, /* 0x46 : interrupt configuration 0->level low detection, 1-> level high, 2-> Out of window, 3->In window, 0x20-> New sample ready , TBC */
0x0b, /* 0x47 : not user-modifiable */
0x00, /* 0x48 : not user-modifiable */
0x00, /* 0x49 : not user-modifiable */
0x02, /* 0x4a : not user-modifiable */
0x0a, /* 0x4b : not user-modifiable */
0x21, /* 0x4c : not user-modifiable */
0x00, /* 0x4d : not user-modifiable */
0x00, /* 0x4e : not user-modifiable */
0x05, /* 0x4f : not user-modifiable */
0x00, /* 0x50 : not user-modifiable */
0x00, /* 0x51 : not user-modifiable */
0x00, /* 0x52 : not user-modifiable */
0x00, /* 0x53 : not user-modifiable */
0xc8, /* 0x54 : not user-modifiable */
0x00, /* 0x55 : not user-modifiable */
0x00, /* 0x56 : not user-modifiable */
0x38, /* 0x57 : not user-modifiable */
0xff, /* 0x58 : not user-modifiable */
0x01, /* 0x59 : not user-modifiable */
0x00, /* 0x5a : not user-modifiable */
0x08, /* 0x5b : not user-modifiable */
0x00, /* 0x5c : not user-modifiable */
0x00, /* 0x5d : not user-modifiable */
0x01, /* 0x5e : not user-modifiable */
0xcc, /* 0x5f : not user-modifiable */
0x0f, /* 0x60 : not user-modifiable */
0x01, /* 0x61 : not user-modifiable */
0xf1, /* 0x62 : not user-modifiable */
0x0d, /* 0x63 : not user-modifiable */
0x01, /* 0x64 : Sigma threshold MSB (mm in 14.2 format for MSB+LSB), use SetSigmaThreshold(), default value 90 mm  */
0x68, /* 0x65 : Sigma threshold LSB */
0x00, /* 0x66 : Min count Rate MSB (MCPS in 9.7 format for MSB+LSB), use SetSignalThreshold() */
0x80, /* 0x67 : Min count Rate LSB */
0x08, /* 0x68 : not user-modifiable */
0xb8, /* 0x69 : not user-modifiable */
0x00, /* 0x6a : not user-modifiable */
0x00, /* 0x6b : not user-modifiable */
0x00, /* 0x6c : Intermeasurement period MSB, 32 bits register, use SetIntermeasurementInMs() */
0x00, /* 0x6d : Intermeasurement period */
0x0f, /* 0x6e : Intermeasurement period */
0x89, /* 0x6f : Intermeasurement period LSB */
0x00, /* 0x70 : not user-modifiable */
0x00, /* 0x71 : not user-modifiable */
0x00, /* 0x72 : distance threshold high MSB (in mm, MSB+LSB), use SetD:tanceThreshold() */
0x00, /* 0x73 : distance threshold high LSB */
0x00, /* 0x74 : distance threshold low MSB ( in mm, MSB+LSB), use SetD:tanceThreshold() */
0x00, /* 0x75 : distance threshold low LSB */
0x00, /* 0x76 : not user-modifiable */
0x01, /* 0x77 : not user-modifiable */
0x0f, /* 0x78 : not user-modifiable */
0x0d, /* 0x79 : not user-modifiable */
0x0e, /* 0x7a : not user-modifiable */
0x0e, /* 0x7b : not user-modifiable */
0x00, /* 0x7c : not user-modifiable */
0x00, /* 0x7d : not user-modifiable */
0x02, /* 0x7e : not user-modifiable */
0xc7, /* 0x7f : ROI center, use SetROI() */
0xff, /* 0x80 : XY ROI (X=Width, Y=Height), use SetROI() */
0x9B, /* 0x81 : not user-modifiable */
0x00, /* 0x82 : not user-modifiable */
0x00, /* 0x83 : not user-modifiable */
0x00, /* 0x84 : not user-modifiable */
0x01, /* 0x85 : not user-modifiable */
0x00, /* 0x86 : clear interrupt, use ClearInterrupt() */
0x00  /* 0x87 : start ranging, use StartRanging() or StopRanging(), If you want an automatic start after VL53L1X_init() call, put 0x40 in location 0x87 */
};

void sensorRead(const uint8_t dataAddr){
	//VL53L1X uses 16 bit addresses
	const uint8_t bufferTX[] = {(dataAddr >> 8) & 0xFF, dataAddr & 0xFF};

	nrf_drv_twi_tx(&m_twi, VL53L1X_ADDR, bufferTX, sizeof(bufferTX), false);	
	nrf_delay_ms(1);

	nrf_drv_twi_rx(&m_twi, VL53L1X_ADDR, &bufferRX, sizeof(bufferRX));
	nrf_delay_ms(1);
}

void sensorWrite(const uint8_t dataAddr, uint8_t data){
	//VL53L1X uses 16 bit addresses
	const uint8_t bufferTX[] = {(dataAddr >> 8) & 0xFF, dataAddr & 0xFF, data};
	nrf_drv_twi_tx(&m_twi, VL53L1X_ADDR, bufferTX, sizeof(bufferTX), false);
	nrf_delay_ms(1);
}

void VL53L1X_StartRanging()
{
	sensorWrite(SYSTEM__MODE_START, 0x40);	/* Enable VL53L1X */

	NRF_LOG_INFO("start ranging: DONE");
    NRF_LOG_FLUSH(); 
}

void VL53L1X_GetInterruptPolarity(uint8_t *pInterruptPolarity)
{
	uint8_t Temp;

	sensorRead(GPIO_HV_MUX__CTRL);
	Temp = bufferRX;
	Temp = Temp & 0x10;
	*pInterruptPolarity = !(Temp>>4);
}

void VL53L1X_CheckForDataReady(uint8_t *isDataReady)
{
	uint8_t Temp;
	uint8_t IntPol;

	VL53L1X_GetInterruptPolarity(&IntPol);
	sensorRead(GPIO__TIO_HV_STATUS);
	Temp = bufferRX;
	/* Read in the register to check if a new value is available */
	
	if ((Temp & 1) == IntPol){
		*isDataReady = 1;
		NRF_LOG_INFO("new distance value: YES");
    	NRF_LOG_FLUSH(); 
	}
	else{
		*isDataReady = 0;
		NRF_LOG_INFO("new distance value: NO");
    	NRF_LOG_FLUSH(); 
	}
}

void VL53L1X_ClearInterrupt()
{
	sensorWrite(SYSTEM__INTERRUPT_CLEAR, 0x01);

	NRF_LOG_INFO("clear interrupt: DONE");
    NRF_LOG_FLUSH(); 
}

void VL53L1X_StopRanging()
{
	sensorWrite(SYSTEM__MODE_START, 0x00);	/* Disable VL53L1X */

	NRF_LOG_INFO("stop ranging: DONE");
    NRF_LOG_FLUSH(); 
}

void VL53L1X_SensorInit()
{
	//soft reset
	sensorWrite(SOFT_RESET, 0x00);
	nrf_delay_us(100);
	sensorWrite(SOFT_RESET, 0x01);

	//wait for booting
	nrf_delay_ms(2000);

	//load default config
	uint16_t Addr = 0x0000;
	for (Addr = 0x002D; Addr <= 0x0087; Addr++){
		sensorWrite(Addr, VL51L1X_DEFAULT_CONFIGURATION[Addr - 0x002d]);
	}
	NRF_LOG_INFO("load default config: DONE");
    NRF_LOG_FLUSH(); 

	VL53L1X_StartRanging();

	uint8_t tmp  = 0;
	while(tmp==0){
			VL53L1X_CheckForDataReady(&tmp);
	}
	
	VL53L1X_ClearInterrupt();
	VL53L1X_StopRanging();
	sensorWrite(VL53L1_VHV_CONFIG__TIMEOUT_MACROP_LOOP_BOUND, 0x09); /* two bounds VHV */
	sensorWrite(0x0B, 0); /* start VHV from the previous temperature */
	NRF_LOG_INFO("end sensor init: DONE");
    NRF_LOG_FLUSH();
	
}

void VL53L1X_GetDistance(uint16_t *distanceValue)
{	
	sensorRead(VL53L1_RESULT__RANGE_STATUS);
	rangeStatus = bufferRX;

	uint8_t distanceValue_HI = 0;
	uint8_t distanceValue_LO = 0;

	sensorRead(VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0_HI);
	distanceValue_HI = bufferRX;
	sensorRead(VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0_LO);
	distanceValue_LO = bufferRX;

	*distanceValue = ((uint16_t)distanceValue_HI << 8) | distanceValue_LO;
}

void VL53L1X_GetRangeStatus(uint8_t *rangeStatusValue)
{
	*rangeStatusValue = 255;
	sensorRead(VL53L1_RESULT__RANGE_STATUS);
	*rangeStatusValue = bufferRX;
}

void doMeasurment(){	

	uint8_t dataReady  = 0;
	while(dataReady==0){
			VL53L1X_CheckForDataReady(&dataReady);
	}
	dataReady = 0;

	VL53L1X_GetRangeStatus(&rangeStatus);
	VL53L1X_GetDistance(&rangeDistance);
}

void sensorStatus(){
	//0 = error/booting
	//3 = ok 
	sensorRead(VL53L1_FIRMWARE__SYSTEM_STATUS);
	if(bufferRX == 3){
		NRF_LOG_INFO("status sensor: OK (%d) ", bufferRX);
    	NRF_LOG_FLUSH(); 
	}
	else{
		NRF_LOG_INFO("status sensor: ERROR (%d) ", bufferRX);
    	NRF_LOG_FLUSH(); 
	}
		
}

#endif